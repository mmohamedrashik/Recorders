package com.example.alphaone.recorders;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

public class MainActivity extends AppCompatActivity {
Button button,button2;
    TextView t;
public  static MediaRecorder myAudioRecorder;
public static String outputFile = null;
private static String Folder = "RazAudio";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = (Button)findViewById(R.id.button);
        button2 = (Button)findViewById(R.id.button2);
        t = (TextView)findViewById(R.id.textView);
       button.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               t.setText(FileName());
               try {
                   myAudioRecorder = new MediaRecorder();
                   myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                   myAudioRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
                   myAudioRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
                   myAudioRecorder.setOutputFile(FileName());

               }catch (Exception e){
                   Log.i("ERROR", e.toString());}

    try{
        myAudioRecorder.prepare();
        myAudioRecorder.start();
    }catch (Exception e){Toast.makeText(getApplicationContext(),e.toString()+"",Toast.LENGTH_SHORT).show(); }
           }
       });
button2.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        myAudioRecorder.stop();
        myAudioRecorder.release();
    }
});
    }
    public String FileName()
    {
        outputFile = Environment.getExternalStorageDirectory().getPath();
        File file = new File(outputFile,Folder);
        if (!file.exists())
        {
            file.mkdir();
        }
        return (file.getAbsolutePath()+"/"+System.currentTimeMillis()+".3gp");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
